# Sibertec\LightspeedADP

A small, easy to use PHP library for accessing Lightspeed ADP data through their API.

#### For more usage samples, look in the [tests](./tests) directory.

#### Format of yaml settings file, if you choose to use one:
```yaml
DealerID: "12345678"
UserName: "YourUsername"
Password: "Y0urP@$$w0rd"
```

```php
<?php
$settings = Sibertec\LightspeedADP\Authentication::LoadFromYaml('/path/to/settings/file.yml');
```
