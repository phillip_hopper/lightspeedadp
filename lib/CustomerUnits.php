<?php


namespace Sibertec\LightspeedADP;


use Exception;
use Sibertec\LightspeedADP\Interfaces\IPart;

class CustomerUnits
{
    /**
     * @param Authentication $auth
     *
     * @return IPart[]
     * @throws Exception
     */
    public static function GetCustomerUnits($auth)
    {
        /** @var IPart[] $parts */
        $parts = Tools::GetAll($auth, DataType::CustomerUnit, null, 'unitheaderid', true);

        return $parts;
    }
}
