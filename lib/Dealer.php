<?php


namespace Sibertec\LightspeedADP;


use Exception;
use stdClass;

class Dealer
{
    /** @var string The DealerID*/
    public $Cmf;

    /** @var string */
    public $DealershipName;

    /** @var string */
    public $DealerNumber;

    /** @var bool */
    public $DirectConnect;

    /**
     * @var int
     * @example A unix timestamp, or NULL
     */
    public $DirectConnectDate;

    /**
     * Dealer constructor.
     *
     * @param stdClass $values
     *
     * @throws Exception
     */
    private function __construct($values)
    {
        $this->Cmf = $values->Cmf ?? null;
        $this->DealershipName = $values->DealershipName ?? null;
        $this->DealerNumber = $values->DealerNumber ?? null;
        $this->DirectConnect = !empty($values->DirectConnect);
        $this->DirectConnectDate = Tools::LightspeedDateToTimestamp($values->DirectConnectDate);
    }

    /**
     * Get the Dealer information objects for this account.
     *
     * @param Authentication $auth
     *
     * @return Dealer[]
     * @throws Exception
     */
    public static function GetDealerInfo($auth)
    {
        $url = 'https://int.lightspeeddataservices.com/lsapi/Dealer';

        /** @var array[] $data */
        $results = Curl::Get($url, $auth);

        /** @var Dealer[] $return_val */
        $return_val = [];

        foreach($results as $result) {
            $return_val[] = new Dealer($result);
        }

        return $return_val;
    }
}
