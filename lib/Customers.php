<?php


namespace Sibertec\LightspeedADP;


use Exception;
use Sibertec\LightspeedADP\Interfaces\ICustomer;

class Customers
{
    /**
     * @param Authentication $auth
     *
     * @return ICustomer[]
     * @throws Exception
     */
    public static function GetCustomers($auth)
    {
        /** @var ICustomer[] $customers */
        $customers = Tools::GetAll($auth, DataType::Customer, null, 'CustomerId', true);

        return $customers;
    }
}
