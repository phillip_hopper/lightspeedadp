<?php


namespace Sibertec\LightspeedADP;


use Exception;
use stdClass;

class License
{
    /** @var string The DealerID*/
    public $Cmf;

    /** @var string */
    public $DealershipName;

    /** @var string */
    public $LicenseKey;


    /**
     * @var int
     * @example A unix timestamp, or NULL
     */
    public $InstallDate;

    /**
     * License constructor.
     *
     * @param stdClass $values
     *
     * @throws Exception
     */
    private function __construct($values)
    {
        $this->Cmf = $values->Cmf ?? null;
        $this->DealershipName = $values->DealershipName ?? null;
        $this->LicenseKey = $values->LicenseKey ?? null;
        $this->InstallDate = Tools::LightspeedDateToTimestamp($values->InstallDate);
    }

    /**
     * Get the License information objects for this account.
     *
     * @param Authentication $auth
     *
     * @return License[]
     * @throws Exception
     */
    public static function GetLicenseInfo($auth)
    {
        $url = 'https://int.lightspeeddataservices.com/lsapi/License/Cmf/' . $auth->DealerID;

        /** @var array[] $data */
        $results = Curl::Get($url, $auth);

        /** @var License[] $return_val */
        $return_val = [];

        foreach($results as $result) {
            $return_val[] = new License($result);
        }

        return $return_val;
    }
}
