<?php


namespace Sibertec\LightspeedADP;


use DateTime;
use Exception;
use stdClass;

class Tools
{
    /** @noinspection PhpUnused */
    /**
     * Generates interface property declarations for the object
     *
     * @param stdClass $obj
     */
    public static function ObjToDoc(stdClass $obj): void
    {
        $template = " * @property %s %s\n";
        $output = '';

        $vars = get_object_vars($obj);
        foreach($vars as $key => $val) {
            $output .= sprintf($template, gettype($val), $key);
        }

        print $output;
    }

    /**
     * Converts a string like '2014-03-10T11:48:04.787' to a unix timestamp
     *
     * @param $date_string
     *
     * @return int|null
     * @throws Exception
     */
    public static function LightspeedDateToTimestamp($date_string): ?int
    {
        if ($date_string == '1000-01-01T00:00:00')
            return null;

        if ($date_string == '1910-01-01T00:00:00')
            return null;

        if (empty($date_string))
            return null;

        if (!str_contains($date_string, '+'))
            $date_string .= '+00:00';

        return (new DateTime($date_string))->getTimestamp();
    }

    /**
     * @param array|stdClass $obj
     *
     * @return stdClass|array
     * @throws Exception
     */
    public static function ConvertDateFields(array|stdClass $obj): array|stdClass
    {
        if (is_object($obj))
            $vars = get_object_vars($obj);
        else
            $vars = $obj;

        foreach($vars as $key => $val) {
            $test_key = strtolower((string)$key);
            if (str_starts_with($test_key, 'date') || str_ends_with($test_key, 'date')) {
                $obj->$key = self::LightspeedDateToTimestamp($val);
                continue;
            }

            if (is_object($val) || is_array($val)) {
                if (is_object($obj))
                    $obj->$key = self::ConvertDateFields($val);
                else
                    $obj[$key] = self::ConvertDateFields($val);
            }
        }

        return $obj;
    }

    /**
     * @param Authentication $auth
     * @param string $type
     * @param string|null $filter
     * @param string|null $order_by Field name for sorting
     * @param bool $convert_dates
     *
     * @return stdClass[]
     * @throws Exception
     */
    public static function GetAll(Authentication $auth, string $type, string $filter=null, string $order_by=null, bool $convert_dates=false): array
    {
        $url = 'https://int.lightspeeddataservices.com/lsapi/' . $type . '/' . $auth->DealerID;
        $skip = 0;
        $top = 500;
        $fields = [];

        if (!empty($order_by))
            $fields['$orderby'] = $order_by;

        /** @var stdClass[] $return_val */
        $return_val = [];

        if (!empty($filter))
            $fields['$filter'] = $filter;

        // top filter
        $fields['$top'] = $top;

        /** @var stdClass[] $results */
        $results = Curl::Get($url, $auth, $fields);

        while (!empty($results)) {

            foreach($results as $result) {

                // do any needed conversions
                if ($convert_dates)
                    $return_val[] = Tools::ConvertDateFields($result);
                else
                    $return_val[] = $result;
            }

            // get the next batch of results
            $skip = $skip + $top;
            $fields['$skip'] = $skip;
            $results = Curl::Get($url, $auth, $fields);
        }

        return $return_val;
    }
}
