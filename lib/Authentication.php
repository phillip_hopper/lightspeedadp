<?php

namespace Sibertec\LightspeedADP;


use Symfony\Component\Yaml\Yaml;

class Authentication
{
    /** @var string */
    public $UserName;

    /** @var string */
    public $Password;

    /** @var string */
    public $DealerID;

    /** @var string Optional. This is the Source ID used by the Lead Integration API */
    public $SourceID;

    /**
     * Authentication constructor.
     *
     * @param string $user_name
     * @param string $password
     * @param string $dealer_id
     * @param string $source_id
     */
    public function __construct($user_name, $password, $dealer_id=null, $source_id=null)
    {
        $this->UserName = $user_name;
        $this->Password = $password;
        $this->DealerID = $dealer_id;
        $this->SourceID = $source_id;
    }

    /**
     * Get an Authentication object initialized from the specified file.
     *
     * @param string $yaml_file_name
     *
     * @return Authentication
     */
    public static function LoadFromYaml($yaml_file_name)
    {
        $settings = Yaml::parseFile($yaml_file_name);
        $dealer_id = $settings['DealerID'] ?? null;
        $source_id = $settings['SourceID'] ?? null;

        return new Authentication($settings['UserName'], $settings['Password'], $dealer_id, $source_id);
    }
}
