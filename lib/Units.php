<?php

namespace Sibertec\LightspeedADP;

use DateTimeInterface;
use Exception;
use Sibertec\LightspeedADP\Interfaces\IUnit;

class Units
{
    /**
     * @param Authentication $auth
     *
     * @return IUnit[]
     * @throws Exception
     */
    public static function GetUnits(Authentication $auth): array
    {
        return Tools::GetAll($auth, DataType::Unit, 'MajorUnitHeaderId gt 0', 'MajorUnitHeaderId', true);
    }

    /**
     * @param Authentication $auth
     * @param int|null $last_updated
     * @return IUnit[]
     * @throws Exception
     */
    public static function GetSoldUnits(Authentication $auth, int $last_updated = null): array
    {
        if (is_null($last_updated))
            return Tools::GetAll($auth, DataType::SoldUnit, 'StockNumber ne \'TEMP\' and MajorUnitHeaderId gt 0', 'MajorUnitHeaderId', true);

        $timestamp = gmdate(DateTimeInterface::ATOM, $last_updated);
        return Tools::GetAll($auth, DataType::SoldUnit, "StockNumber ne 'TEMP' and lastupdatedate gt datetime'$timestamp'", 'MajorUnitHeaderId', true);
    }
}
