<?php


namespace Sibertec\LightspeedADP;


use DateTime;
use DateTimeZone;
use Exception;
use Sibertec\LightspeedADP\Interfaces\IDealDetail;

class DealsDetail
{
    /**
     * @param Authentication $auth
     *
     * @return IDealDetail[]
     * @throws Exception
     */
    public static function GetDealsDetail($auth)
    {
        /** @var IDealDetail[] $deals_detail */
        $deals_detail = Tools::GetAll($auth, DataType::DealDetail, null, 'FinInvoiceId', true);

        return $deals_detail;
    }

    /**
     * @param Authentication $auth
     * @param int $start_timestamp
     *
     * @return IDealDetail[]
     * @throws Exception
     */
    public static function GetUpdatedDealsDetail($auth, int $start_timestamp)
    {
        $utc = new DateTime('now', new DateTimeZone('UTC'));
        $utc->setTimestamp($start_timestamp);
        $filter_val = $utc->format('Y-m-d\TH:i:s');

        /** @var IDealDetail[] $deals_detail */
        $deals_detail = Tools::GetAll($auth, DataType::DealDetail, "FinanceDate gt datetime'{$filter_val}'",'FinanceDate', true);

        return $deals_detail;
    }
}
