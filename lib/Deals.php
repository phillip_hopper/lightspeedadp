<?php


namespace Sibertec\LightspeedADP;


use Exception;
use Sibertec\LightspeedADP\Interfaces\IDeal;

class Deals
{
    /**
     * @param Authentication $auth
     *
     * @return IDeal[]
     * @throws Exception
     */
    public static function GetDeals($auth)
    {
        /** @var IDeal[] $deals */
        $deals = Tools::GetAll($auth, DataType::Deal, null, 'FinInvoiceId', true);

        return $deals;
    }
}
