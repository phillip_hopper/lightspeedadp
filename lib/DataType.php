<?php


namespace Sibertec\LightspeedADP;


abstract class DataType
{
    const Unit = 'Unit';
    const SoldUnit = 'SoldUnit';
    const Part = 'Part';
    const Customer = 'Customer';
    const CustomerUnit = 'CustomerUnit';
    const Deal = 'Deal';
    const DealDetail = 'DealDetail';
}
