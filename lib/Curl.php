<?php


namespace Sibertec\LightspeedADP;


use Exception;
use Html2Text\Html2Text;


class Curl
{
    private static array $rate_window = [];

    /**
     * Return the results of an HTTP GET request.
     *
     * @param string $url
     * @param Authentication $auth
     * @param array|null $fields
     * @param bool $as_json
     *
     * @return mixed
     * @throws Exception
     */
    public static function Get(string $url, Authentication $auth, array $fields=null, bool $as_json=true): mixed
    {
        $headers = array(
            'Accept: application/json, */*',
            'Cache-Control: no-cache',
            'Connection: Keep-Alive'
        );

        if (!empty($fields)) {
            $qs = http_build_query($fields);
            if (!str_contains($url, '?'))
                $url .= '?' . $qs;
            else
                $url .= '&' . $qs;
        }

        $response = self::DoCurl($url, $headers, null, $auth->UserName, $auth->Password);

        if (!$as_json)
            return $response;

        return self::ResponseToJson($response);
    }

    /**
     * Execute a HTTP request using curl.
     *
     * @param string $url
     * @param array $headers
     * @param array|null $params
     * @param string $username
     * @param string $password
     *
     * @return string|bool
     * @throws Exception
     */
    public static function DoCurl(string $url, array $headers, ?array $params, string $username, string $password): string|bool
    {
        // push this call into the rate window
        self::$rate_window[] = time();

        // The documentation says to keep the requests per minute under 200.
        // Doing it this way will result in a theoretical max of 200 per minute.
        // The actual max will be less, due to network latency and time used to process the data received.
        if (self::CalculateRatePerMinute() > 100) {

            // pause for 0.6 seconds
            usleep(600000);
        }

        $ch = curl_init($url);

        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,60); // 60 second connection timeout
        curl_setopt($ch, CURLOPT_TIMEOUT, 300);       // 5 minute function timeout
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        if (!empty($username))
            curl_setopt($ch, CURLOPT_USERPWD, $username . ":" . $password);

        if (!empty($params)) {
            foreach($params as $key => $value) {
                curl_setopt($ch, $key, $value);
            }
        }

        $content = curl_exec($ch);
        $err = curl_errno($ch);
        $errmsg = curl_error($ch);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if (!empty($err))
            throw new Exception($errmsg);

        if ($http_code > 399) {

            $err = json_decode($content);

            if (!empty($err->ExceptionMessage))
                throw new Exception('HTTP error ' . $http_code . ': ' . $err->ExceptionMessage . PHP_EOL, $http_code);
            elseif (!empty($err->Message))
                throw new Exception('HTTP error ' . $http_code . ': ' . $err->Message . PHP_EOL, $http_code);
            else
                throw new Exception('HTTP error ' . $http_code . ': ' . trim((new Html2Text($content))->getText()) . PHP_EOL, $http_code);
        }

        return $content;
    }

    /**
     * Convert the response into a stdClass.
     *
     * @param string $response
     *
     * @return mixed
     * @throws Exception
     */
    private static function ResponseToJson(string $response): mixed
    {
        $json = json_decode($response, false);

        if (is_null($json))
            throw new Exception("The response received was not decodable as json:\n$json");

        return $json;
    }

    private static function CalculateRatePerMinute(): int
    {
        // remove all entries older than 1 minute
        self::$rate_window = array_filter(self::$rate_window, function($v) { return $v > (time() - 60); });

        // return the number of calls in the last minute
        return count(self::$rate_window);
    }
}
