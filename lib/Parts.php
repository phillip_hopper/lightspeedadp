<?php


namespace Sibertec\LightspeedADP;


use Exception;
use Sibertec\LightspeedADP\Interfaces\IPart;

class Parts
{
    /**
     * @param Authentication $auth
     *
     * @return IPart[]
     * @throws Exception
     */
    public static function GetParts($auth)
    {
        /** @var IPart[] $parts */
        $parts = Tools::GetAll($auth, DataType::Part, 'PartNumber gt \'0\'', 'PartNumber', true);

        return $parts;
    }
}
