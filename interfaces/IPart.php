<?php


namespace Sibertec\LightspeedADP\Interfaces;


/**
 * Interface IPart - generated 26 SEP 2019
 *
 * @property string Cmf
 * @property string DealerId
 * @property string PartNumber
 * @property string SupplierCode
 * @property integer DateGathered
 * @property string Description
 * @property integer OnHand
 * @property integer Avail
 * @property integer OnOrder
 * @property integer OnOrderAvail
 * @property integer LastSoldDate
 * @property integer LastReceivedDate
 * @property integer LastAdjustmentDate
 * @property string ReOrderMethod
 * @property integer MinimumQty
 * @property integer MaximumQty
 * @property double Cost
 * @property double CurrentActivePrice
 * @property string OrderUnit
 * @property integer OrderUnitQty
 * @property double Retail
 * @property integer LastCountDate
 * @property string SupersededTo
 * @property string UPC
 * @property string Bin1
 * @property string Bin2
 * @property string Bin3
 * @property string category
 * @property integer unitheaderid
 *
 * @package Interfaces
 */
interface IPart
{
}
