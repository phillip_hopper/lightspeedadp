<?php


namespace Sibertec\LightspeedADP\Interfaces;


/**
 * Interface IDealer - generated 26 SEP 2019
 *
 * @property double totalCost
 * @property string Cmf
 * @property string DealerId
 * @property integer MajorUnitHeaderId
 * @property int DateGathered
 * @property int lastupdatedate
 * @property string StockNumber
 * @property string NewUsed Either "N" or "U"
 * @property integer ModelYear
 * @property string Make
 * @property string Model
 * @property string VIN
 * @property string Class
 * @property string CodeName
 * @property string Location
 * @property string StoreLocation
 * @property string Odometer
 * @property integer Cylinders
 * @property double HP
 * @property string BodyStyle
 * @property string Color
 * @property string Condition
 * @property int InvoiceDate
 * @property double InvoiceAmt
 * @property double PackAmt
 * @property double HoldbackAmt
 * @property double FreightCost
 * @property double DSRP
 * @property double MSRP
 * @property string OnHold
 * @property integer PackageId
 * @property int DateReceived
 * @property double Length
 * @property double Height
 * @property double Width
 * @property double Draft
 * @property double Beam
 * @property integer GVWR
 * @property integer GDW
 * @property string InteriorColor
 * @property string ExteriorColor
 * @property string Manufacturer
 * @property string UnitType
 * @property string FloorLayout
 * @property string FuelType
 * @property string UnitName
 * @property string HullConstruction
 * @property string Comments
 * @property string UnitStatus
 * @property string UserDefined1
 * @property string UserDefined2
 * @property string UserDefined3
 * @property string UserDefined4
 * @property string UserDefined5
 * @property string UserDefined6
 * @property string UserDefined7
 * @property string UserDefined8
 * @property string UserDefined9
 * @property string UserDefined10
 * @property double UserDefined11
 * @property double UserDefined12
 * @property double UserDefined13
 * @property double UserDefined14
 * @property double UserDefined15
 * @property integer UserDefined16
 * @property integer UserDefined17
 * @property integer UserDefined18
 * @property integer UserDefined19
 * @property integer UserDefined20
 * @property string UserDefined1desc
 * @property string UserDefined2desc
 * @property string UserDefined3desc
 * @property string UserDefined4desc
 * @property string UserDefined5desc
 * @property string UserDefined6desc
 * @property string UserDefined7desc
 * @property string UserDefined8desc
 * @property string UserDefined9desc
 * @property string UserDefined10desc
 * @property string UserDefined11desc
 * @property string UserDefined12desc
 * @property string UserDefined13desc
 * @property string UserDefined14desc
 * @property string UserDefined15desc
 * @property string UserDefined16desc
 * @property string UserDefined17desc
 * @property string UserDefined18desc
 * @property string UserDefined19desc
 * @property string UserDefined20desc
 * @property integer WebUnit
 * @property string WebTitle
 * @property string WebDescription
 * @property double WebPrice
 * @property integer WebPriceHidden
 * @property string titlestatus
 * @property string trimcolor
 * @property string unitcondition Something like "EXCELLENT"
 * @property string carbcompliance
 * @property string drivetype
 * @property string enginecycles
 * @property string powertype
 * @property string starttype
 * @property string numacunits
 * @property string sleepcapacity
 * @property string barlength
 * @property string bladelength
 * @property string cuttingwidth
 * @property string videourl
 * @property string numslideouts
 * @property string turningradius
 * @property string flooredby
 * @property string floorplan
 * @property string planstartingdate
 * @property string planendingdate
 * @property double originalamt
 * @property string originaldate
 * @property double curtailmentamt
 * @property string curtailmentdate
 * @property double flooredbalance
 * @property double Greywatertankcapacity
 * @property double Blackwatertankcapacity
 * @property double Freight
 * @property string Ordernumber
 * @property string Ordereta
 * @property double Estimatedsetupamt
 * @property double Partsprepamount
 * @property double Partsprepcost
 * @property double Discountpercentagefromdsrp
 * @property double Priceprotectionrebate
 * @property double Manufacturertocustomerrebate
 * @property double Manufacturertodealerrebate
 * @property double Optionsamount
 * @property double Optionscost
 * @property string Hitchtype
 * @property double Axlenumber
 * @property double Axlecapacity
 * @property string Fueltype
 * @property double Cylinders2
 * @property double Cylinders3
 * @property string Propulsion
 * @property string Floorlayout
 * @property double Hitchdryweight
 * @property double Numberofwheels
 * @property string Wheelsize
 * @property double Fuelcapacity
 * @property double Freshwatertankcapacity
 * @property double Maxcarrycapacity
 * @property string Engine1displacement
 * @property string Engine2displacement
 * @property string Engine3displacement
 * @property double HP2
 * @property double HP3
 * @property double Marginprotectionamount
 *
 * @property IUnitPart[] Parts
 * @property IOption[] Options
 * @property ILabor[] Labor
 * @property IImage[] Images
 *
 * @package Interfaces
 */
interface IUnit
{
}
