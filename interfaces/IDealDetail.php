<?php


namespace Sibertec\LightspeedADP\Interfaces;


/**
 * Interface IDealDetail - generated 27 SEP 2019
 *
 * @property string Cmf
 * @property string DealerId
 * @property string DealNo
 * @property integer FinInvoiceId
 * @property integer CommonInvoiceID
 * @property integer FinanceDate
 * @property integer OriginatingDate
 * @property integer DeliveryDate
 * @property string salesmanid
 * @property double clpremium
 * @property double clcost
 * @property double ahpremium
 * @property double ahcost
 * @property string CustID
 * @property string lienholder
 * @property double AmtFinanced
 * @property integer Term
 * @property double Rate
 * @property double Payment
 * @property double totaldownpayment
 * @property double fincharge
 * @property double fincost
 * @property integer DaysToFirst
 * @property string SalesmanName
 * @property string lienaddressfirstline
 * @property string lienaddresssecondline
 * @property string liencity
 * @property string lienzip
 * @property string lienstate
 * @property string cobuyercustid
 * @property string cobuyername
 * @property string cobuyeraddr
 * @property string cobuyeraddr2
 * @property string cobuyercity
 * @property string cobuyerstate
 * @property string cobuyerzip
 * @property string cobuyercounty
 * @property string cobuyerhomephone
 * @property string cobuyerworkphone
 * @property string cobuyercellphone
 * @property string cobuyeremail
 * @property integer cobuyerbirthdate
 * @property string Salesman2name
 * @property string Salesman2id
 * @property string Salesmanfi1id
 * @property string Salesmanfi1name
 * @property string Salesmanfi2id
 * @property string Salesmanfi2name
 * @property double Fincostoverride
 * @property IDealDetailUnit[] Units
 * @property IDealDetailTrade[] Trade
 * @property IDealDetailDealExtraLine[] DealExtraLines
 *
 * @package Interfaces
 */
interface IDealDetail
{

}
