<?php


namespace Sibertec\LightspeedADP\Interfaces;


/**
 * Interface IDealDetailTrade - generated 27 SEP 2019
 *
 * @property string DealerId
 * @property string VIN
 * @property string StdMake
 * @property string StdModel
 * @property string StdYear
 * @property string StdCategory
 * @property double allowance
 * @property double acv
 * @property double payoff
 *
 * @package Interfaces
 */
interface IDealDetailTrade
{
}
