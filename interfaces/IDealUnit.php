<?php


namespace Sibertec\LightspeedADP\Interfaces;


/**
 * Interface IDealUnit - generated 26 SEP 2019
 *
 * @property string DealerId
 * @property integer DealUnitId
 * @property string Newused
 * @property string Year
 * @property string Make
 * @property string Model
 * @property string VIN
 * @property string Class
 * @property double Unitprice
 * @property double UnitSoldPrice
 * @property double TotalPartsAmount
 *
 * @package Interfaces
 */
interface IDealUnit
{

}
