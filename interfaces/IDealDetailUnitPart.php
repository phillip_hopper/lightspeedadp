<?php


namespace Sibertec\LightspeedADP\Interfaces;


/**
 * Interface IDealDetailUnitPart - generated 27 SEP 2019
 *
 * @property string DealerId
 * @property string PartNumber
 * @property string SupplierCode
 * @property integer Qty
 * @property double Cost
 * @property double Price
 * @property double Retail
 * @property string SetupInstall
 * @property string Description
 *
 * @package Interfaces
 */
interface IDealDetailUnitPart
{
}
