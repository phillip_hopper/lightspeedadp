<?php


namespace Sibertec\LightspeedADP\Interfaces;


/**
 * Interface IDealDetailDealExtraLine - generated 27 SEP 2019
 *
 * @property string DealerId
 * @property integer DealUnitId
 * @property string FinInvoiceId
 * @property string LineName
 * @property string ExtraLineNumber
 * @property string LineType
 * @property double Amount
 * @property double Cost
 * @property double Term
 *
 * @package Interfaces
 */
interface IDealDetailDealExtraLine
{
}
