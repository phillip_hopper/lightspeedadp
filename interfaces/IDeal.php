<?php


namespace Sibertec\LightspeedADP\Interfaces;


/**
 * Interface IDeal - generated 26 SEP 2019
 *
 * @property string Cmf
 * @property string DealerId
 * @property string DealNo
 * @property integer FinInvoiceId
 * @property integer FinanceDate
 * @property integer OriginatingDate
 * @property integer DeliveryDate
 * @property string custid
 * @property IDealUnit[] Units
 *
 * @package Interfaces
 */
interface IDeal
{
}
