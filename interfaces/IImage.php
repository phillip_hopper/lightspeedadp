<?php


namespace Sibertec\LightspeedADP\Interfaces;


/**
 * Interface IImage - generated 26 SEP 2019
 *
 * @property string DealerId
 * @property boolean PrimaryImage
 * @property string ImageUrl
 *
 * @package Interfaces
 */
interface IImage
{
}
