<?php


namespace Sibertec\LightspeedADP\Interfaces;


/**
 * Interface ICustomer - generated 26 SEP 2019
 *
 * @property string Cmf
 * @property string DealerId
 * @property integer CustomerId
 * @property integer DateGathered
 * @property string CustFullName
 * @property string FirstName
 * @property string MIddleName
 * @property string LastName
 * @property string Companyname
 * @property string Address1
 * @property string Address2
 * @property string City
 * @property string State
 * @property string Zip
 * @property string County
 * @property string Country
 * @property string HomePhone
 * @property string WorkPhone
 * @property string CellPhone
 * @property string EMail
 * @property integer Birthdate
 * @property integer HasDriversLicenseNumber
 * @property string CustomerType
 * @property integer LoyaltyCustomer
 *
 * @package Interfaces
 */
interface ICustomer
{

}
