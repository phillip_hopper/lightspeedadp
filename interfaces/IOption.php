<?php


namespace Sibertec\LightspeedADP\Interfaces;


/**
 * Interface IOption - generated 26 SEP 2019
 *
 * @property string DealerId
 * @property string Description
 * @property double Price
 * @property string OptionType
 *
 * @package Interfaces
 */
interface IOption
{
}
