<?php


namespace Sibertec\LightspeedADP\Interfaces;


/**
 * Interface IDealDetailUnit - generated 27 SEP 2019
 *
 * @property string DealerId
 * @property integer DealUnitId
 * @property string Newused
 * @property string Year
 * @property string Make
 * @property string Model
 * @property string VIN
 * @property string Class
 * @property double Unitprice
 * @property double mucost
 * @property double Freight
 * @property double Freightcost
 * @property double Handling
 * @property double Handlingcost
 * @property double LicFees
 * @property double LicFeesCost
 * @property double Totaccy
 * @property double Totinstall
 * @property double Tradeall
 * @property double Tradeacv
 * @property double Accycost
 * @property double Installcost
 * @property double docfees
 * @property double actualcost
 * @property string DateReceived
 * @property double servcont
 * @property double sccost
 * @property double propliab
 * @property double plcost
 * @property double PackAmt
 * @property double HoldbackAmt
 * @property string unittype
 * @property string SalesType
 * @property double UnitSoldPrice
 * @property double UnitSoldCost
 * @property double TotalPartsAmount
 * @property integer DaysInStore
 * @property double UnitLine1
 * @property double UnitLine2
 * @property double UnitLine3
 * @property double UnitLine4
 * @property double UnitLine5
 * @property double UnitLine6
 * @property double UnitLine7
 * @property double UnitLine8
 * @property double UnitLine9
 * @property double UnitLine10
 * @property double UnitLine11
 * @property double UnitLine12
 * @property string Color
 * @property string Odometer
 * @property string stocknumber
 * @property double deposit
 * @property string bodytype
 * @property string enginenumber
 * @property string cylinders
 * @property string gvwr
 * @property string fueltype
 * @property IDealDetailUnitPart[] Parts
 *
 * @package Interfaces
 */
interface IDealDetailUnit
{
}
