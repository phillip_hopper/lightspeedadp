<?php


namespace Sibertec\LightspeedADP\Interfaces;


/**
 * Interface ILabor - generated 26 SEP 2019
 *
 * @property string DealerId
 * @property integer MajorUnitHeaderId
 * @property integer MajorUnitLaborId
 * @property string Description
 * @property string SetupInstall
 * @property double Cost
 * @property double Price
 * @property double SubletCost
 * @property double SubletPrice
 *
 * @package Interfaces
 */
interface ILabor
{
}
