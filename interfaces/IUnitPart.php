<?php


namespace Sibertec\LightspeedADP\Interfaces;


/**
 * Interface IUnitPart - generated 26 SEP 2019
 *
 * @property string DealerId
 * @property string Description
 * @property string SetupInstall
 * @property integer Qty
 * @property double Cost
 * @property double Price
 *
 * @package Interfaces
 */
interface IUnitPart
{
}
