<?php
/** @noinspection PhpUnreachableStatementInspection */
/** @noinspection PhpUnhandledExceptionInspection */


use PHPUnit\Framework\TestCase;
use Sibertec\LightspeedADP\Authentication;
use Sibertec\LightspeedADP\Deals;
use Sibertec\LightspeedADP\DealsDetail;


class DealsTest extends TestCase
{
    public function testGetDeals()
    {
        $this->markTestSkipped();

        $auth = Authentication::LoadFromYaml(__DIR__ . '/config/lightspeed_data.testing.yml');

        $deals = Deals::GetDeals($auth);

        $this->assertNotEmpty($deals);
        $this->assertNotEmpty($deals[0]->DealNo);
    }

    public function testGetDealsDetail()
    {
        $auth = Authentication::LoadFromYaml(__DIR__ . '/config/lightspeed_data.testing.yml');

        $deals_detail = DealsDetail::GetDealsDetail($auth);

        $this->assertNotEmpty($deals_detail);
        $this->assertNotEmpty($deals_detail[0]->DealNo);
    }

    public function testGetUpdatedDealsDetail()
    {
        $auth = Authentication::LoadFromYaml(__DIR__ . '/config/lightspeed_data.testing.yml');

        $deals_detail = DealsDetail::GetUpdatedDealsDetail($auth, strtotime('today - 1 month'));

        $this->assertNotEmpty($deals_detail);
        $this->assertNotEmpty($deals_detail[0]->DealNo);
    }
}
