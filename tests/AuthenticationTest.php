<?php


use PHPUnit\Framework\TestCase;
use Sibertec\LightspeedADP\Authentication;

class AuthenticationTest extends TestCase
{
    public function testGetDataSettings()
    {
        $auth = Authentication::LoadFromYaml(__DIR__ . '/config/lightspeed_data.testing.yml');

        $this->assertNotEmpty($auth);
        $this->assertNotEmpty($auth->DealerID);
        $this->assertNotEmpty($auth->UserName);
        $this->assertNotEmpty($auth->Password);
    }
}
