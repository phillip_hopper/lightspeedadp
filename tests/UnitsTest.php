<?php /** @noinspection PhpUnhandledExceptionInspection */


use PHPUnit\Framework\TestCase;
use Sibertec\LightspeedADP\Authentication;
use Sibertec\LightspeedADP\Interfaces\IUnit;
use Sibertec\LightspeedADP\Units;

class UnitsTest extends TestCase
{
    public function testGetUnits()
    {
        $auth = Authentication::LoadFromYaml(__DIR__ . '/config/lightspeed_data.testing.yml');

        /** @var IUnit[] $units */
        $units = Units::GetUnits($auth);

        $this->assertNotEmpty($units);
        $this->assertNotEmpty($units[0]->MajorUnitHeaderId);
    }

    public function testGetSoldUnits()
    {
        $auth = Authentication::LoadFromYaml(__DIR__ . '/config/lightspeed_data.testing.yml');

        /** @var IUnit[] $units */
        $units = Units::GetSoldUnits($auth);

        $this->assertNotEmpty($units);
        $this->assertNotEmpty($units[0]->MajorUnitHeaderId);
    }
}
