<?php /** @noinspection PhpUnhandledExceptionInspection */


use PHPUnit\Framework\TestCase;
use Sibertec\LightspeedADP\Authentication;
use Sibertec\LightspeedADP\CustomerUnits;

class CustomerUnitsTest extends TestCase
{
    public function testGetCustomerUnits()
    {
        $auth = Authentication::LoadFromYaml(__DIR__ . '/config/lightspeed_data.testing.yml');

        $parts = CustomerUnits::GetCustomerUnits($auth);

        $this->assertNotEmpty($parts);
        $this->assertNotEmpty($parts[0]->unitheaderid);
    }
}
