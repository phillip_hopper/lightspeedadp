<?php
/** @noinspection PhpUnreachableStatementInspection */
/** @noinspection PhpUnhandledExceptionInspection */


use PHPUnit\Framework\TestCase;
use Sibertec\LightspeedADP\Authentication;
use Sibertec\LightspeedADP\Customers;
use Sibertec\LightspeedADP\Interfaces\ICustomer;

class CustomersTest extends TestCase
{
    public function testGetCustomers()
    {
        $this->markTestSkipped();

        $auth = Authentication::LoadFromYaml(__DIR__ . '/config/lightspeed_data.testing.yml');

        /** @var ICustomer[] $customers */
        $customers = Customers::GetCustomers($auth);

        $this->assertNotEmpty($customers);
        $this->assertNotEmpty($customers[0]->CustomerId);
    }
}
