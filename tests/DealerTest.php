<?php /** @noinspection PhpUnhandledExceptionInspection */


use PHPUnit\Framework\TestCase;
use Sibertec\LightspeedADP\Authentication;
use Sibertec\LightspeedADP\Dealer;


class DealerTest extends TestCase
{
    public function testGetDealerInfo()
    {
        $auth = Authentication::LoadFromYaml(__DIR__ . '/config/lightspeed_data.testing.yml');

        $dealers = Dealer::GetDealerInfo($auth);

        $this->assertNotEmpty($dealers);
        $this->assertNotEmpty($dealers[0]->Cmf);
    }
}
