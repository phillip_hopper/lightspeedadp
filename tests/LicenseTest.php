<?php
/** @noinspection PhpUnreachableStatementInspection */
/** @noinspection PhpUnhandledExceptionInspection */


use PHPUnit\Framework\TestCase;
use Sibertec\LightspeedADP\Authentication;
use Sibertec\LightspeedADP\License;


class LicenseTest extends TestCase
{
    public function testGetLicenseInfo()
    {
        $this->markTestSkipped();

        $auth = Authentication::LoadFromYaml(__DIR__ . '/config/lightspeed_data.testing.yml');

        $licenses = License::GetLicenseInfo($auth);

        $this->assertNotEmpty($licenses);
        $this->assertNotEmpty($licenses[0]->LicenseKey);
    }
}
