<?php /** @noinspection PhpUnhandledExceptionInspection */


use PHPUnit\Framework\TestCase;
use Sibertec\LightspeedADP\Authentication;
use Sibertec\LightspeedADP\Interfaces\IPart;
use Sibertec\LightspeedADP\Parts;

class PartsTest extends TestCase
{
    public function testGetParts()
    {
        $auth = Authentication::LoadFromYaml(__DIR__ . '/config/lightspeed_data.testing.yml');

        /** @var IPart[] $parts */
        $parts = Parts::GetParts($auth);

        $this->assertNotEmpty($parts);
        $this->assertNotEmpty($parts[0]->PartNumber);
    }
}
